package it.redss.starter.amazon;

import com.netflix.appinfo.AmazonInfo;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@Profile("ecs")
@Configuration
public class EcsAwareEurekaInstanceAutoConfiguration {

  private static Logger LOG = LoggerFactory
      .getLogger(EcsAwareEurekaInstanceAutoConfiguration.class);
  EurekaInstanceConfigBean config;
  @Autowired
  private Environment env;
  @Value("${ECS_HOST_PORT}")
  private String ecsHostServerPort;


  public EcsAwareEurekaInstanceAutoConfiguration(EurekaInstanceConfigBean config) {
    this.config = config;
  }

  @PostConstruct
  private void setupInstanceInformation() {
    AmazonInfo info = AmazonInfo.Builder.newBuilder().autoBuild("eureka");

    if (info.getId() != null) {
      config.setDataCenterInfo(info);
      info.getMetadata().put(
          AmazonInfo.MetaDataKey.publicHostname.getName(),
          info.get(AmazonInfo.MetaDataKey.publicIpv4));
      config.setHostname(info.get(AmazonInfo.MetaDataKey.publicHostname));
      config.setIpAddress(info.get(AmazonInfo.MetaDataKey.publicIpv4));

      if (ecsHostServerPort != null) {
        config.setNonSecurePortEnabled(true);
        config.setNonSecurePort(Integer.parseInt(ecsHostServerPort));
      }
    }

  }


}