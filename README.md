# ecs-eureka-auto-configuration #

Simple autoconfiguration supporting Eureka in ECS.

### Usage ###

Usage of this module requires the image from [ecs-java-eureka](https://bitbucket.org/RedSoftwareSystems/ecs-java-eureka) project. 

First step is to include the library in the eureka instance as a dependency:
 
    <dependencies>
    ...
      <dependency>
        <groupId>it.redss</groupId>
        <artifactId>ecs-eureka-auto-configuration</artifactId>
        <version>1.0.0-SNAPSHOT</version>
      </dependency> 
    ...
    </dependencies>	

As you can check from code, an environment variable `ECS_HOST_PORT` is expected. This variable is injected into instance configuration
with a `@PostConstruct` method, which also injects AWS configuration details.

To be stressed that this configuration is applied only if the spring boot profile contains `ecs` - eg, you can combine multiple profiles but
an `ecs` one must be declared.

In the `application.yml` I added the following information for the `ecs` profile:
    
    eureka:
      environment: test
      datacenter: aws
      client:
        register-with-eureka: true
        fetch-registry: true
        serviceUrl:
          defaultZone: http://eureka.server.address:8761/eureka
      instance:
        hostname: instance_host_nome
        prefer-ip-address: true

The criticality here is the `prefer-ip-address: true` option.

The [ecs-java-eureka](https://bitbucket.org/RedSoftwareSystems/ecs-java-eureka) is a Docker image derived from the excellent and widely suggested
`frolvlad/alpine-oraclejdk8:slim` docker image: it adds python and some modules (nominally, boto, boto3 and requests ) and an entry point script.

The entry point expects the definition of a CMD inside the final (yours) docker definition. Following is an example docker definition:

    FROM redss/ecs-java-eureka:0.1-alpha
    VOLUME /tmp
    
    MAINTAINER Somebody <somebody@somewhere>
    
    ADD ./applications.jar /app/
    RUN sh -c 'touch /app/gip-db-service.jar'
    
    CMD ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/application.jar"]
    
    EXPOSE port#


### Acknowledgements ###

This project borrows ideas from [aws-aware-eureka-instance](https://gitlab.com/ixilon/aws-aware-eureka-instance)  by
Gerald Fiedler.

### Contact ###

Please contact lbotti@red.software.systems for info about this
project.
